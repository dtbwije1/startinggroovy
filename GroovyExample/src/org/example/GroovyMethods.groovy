package org.example

class GroovyMethods {
    //Groovy pass values to methods as oppose to java which passes the reference.
    static void main(String[] args){
        passByValue("Test");
        def newName = "Tharanga";
        passByValue(newName);

    }

    static void passByValue(name){
        name = "wrong name it is";
        println(" %s is my name", name);
    }


}
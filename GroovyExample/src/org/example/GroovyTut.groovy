package org.example

class GroovyTut {
	static void main(String[] args) {
		println("Hello World");
		println(5.6.plus(4));
		println(5.multiply(5.7.divide(2)));
		def randString = "RANDOM";
		printf("%15s  is wrong \n", randString);
		printf("%-15s  is wrong \n", randString);
	}
}
